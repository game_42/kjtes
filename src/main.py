import os
import cv2
import numpy as np
import pytesseract
import backward
from tkinter import *
from tkinter import messagebox
from functools import partial
import time
from PIL import ImageGrab
from pynput.mouse import Controller, Button
import Stack
import copy
import random

_test = False

class Game():
	def __init__(self,image):

		self.x_start = 28
		self.y_start = 88
		self.num_width = 21

		self.dis = 36
		self.imagelists = []
		self.center_pos = []

		x_dis = self.dis
		y_dis = self.dis

		y_start = self.y_start
		y_end = self.y_start + self.num_width
		
		for i in range(16):
			x_start = self.x_start
			x_end = x_start + self.num_width
			imgelist = list()
			center_pos = []
			for j in range(10):
				every_num = image[y_start:y_end,x_start:x_end]
				every_num = cv2.resize(every_num, (28, 28), interpolation=cv2.INTER_AREA)
				_, every_num = cv2.threshold(every_num, 127, 255, cv2.THRESH_BINARY)
				
				center_pos.append((x_start + self.num_width/2,y_start + self.num_width/2))
				imgelist.append(every_num)
				x_start += x_dis
				x_end   += x_dis

			self.imagelists.append(imgelist)
			self.center_pos.append(center_pos)
			y_start += y_dis
			y_end   += y_dis

	def get_Pix_Position(self,i,j):
		return self.center_pos[i][j]

	def get_image(self,i,j):
		return self.imagelists[i][j]

class Tes:
	def __init__(self):
		self.temp_count = None
		self.max_size = 0
		self.min_size = 10000000

		self.current_step = 0
		self.all_setps = []
		self.setps = Stack.Stack()
		self.score = 0

		self.game = None
		self.x_start = 0 
		self.y_start = 0 

		self.imagelists = None
		self.current_result = Stack.Stack()
		self.result = []
		self.map = None
		self.ori_map = []
			
	def pre_img(self):
		self.imagelists = self.imagelists.reshape(160, 28,28)
		self.imagelists = self.imagelists.astype('float32')
		self.imagelists /= 255.0

	def load_data(self,img = None):

		if _test == False:
			image = ImageGrab.grab(bbox=(0, 0, 1440, 900))
			image = np.array(image.getdata(), np.uint8).reshape(image.size[1], image.size[0], 4)
			image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
			cv2.imwrite("/Users/a58/Desktop/tes/img/6.jpg",image)
		else:
			image = cv2.imread(img)
			image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

		x_start = 520
		x_end = x_start + 400
		y_start = 100
		y_end = y_start + 700
	
		self.x_start = x_start
		self.y_start = y_start
		image = image[y_start:y_end,x_start:x_end]

		self.game = Game(image)

	def set_steps(self,si,sj,ei,ej):
		current_step = 0

		for i in range(si,ei+1):
			for j in range(sj,ej+1):
				current_step = current_step | (1<<(i * 10 + j))
		current_step = self.current_step | current_step

		for st in self.all_setps:
			if st == current_step:
				return False

		for i in range(si,ei + 1 ):
			for j in range(sj, ej + 1):
				self.map[i][j] = 0 

		self.current_step = current_step
		self.current_result.push((si,sj,ei,ej))
				
		self.setps.push(self.current_step)
		self.all_setps.append(self.current_step)

		return True

	def debug(self,last_step):
		print("----------------------=======--------")
		# last_step = self.setps.peek()
		for i in range(16):
			for j in range(10):
				if last_step & (1<<(i * 10 + j)) == 0:
					print(0,end=" ")
				else:
					print(1,end=" ")
			print("")
		print("----------------------=======--------")

	def find_duplicate(self,nums):
		res = []
		for num in nums:
			if nums.count(num) > 1 and num not in res:
				print("00000000000000000000000000000000000000")
				exit(0)


	def backup(self):
		self.setps.pop()
		last_result = self.current_result.pop()

		if self.setps.is_empty():
			self.current_step = 0
			self.map = copy.deepcopy(self.ori_map) 
			return 0

		last_step = self.setps.peek()
		self.current_step = last_step
		
		for i in range(16):
			for j in range(10):
				if last_step & (1<<(i * 10 + j)) == 0:
					self.map[i][j] = self.ori_map[i][j] 
				else:
					self.map[i][j] = 0
		if self.max_size < self.setps.size():
			self.max_size = self.setps.size()
			print("-------bbbbbbbbbbbbbbbbbbbbb",self.max_size,self.min_size,self.setps.size())

		if self.min_size > self.setps.size():
			self.min_size = self.setps.size()
			print("-------bbbbbbbbbbbbbbbbbbbbb",self.max_size,self.min_size,self.setps.size())
		
		return last_result
		

	def comput_ij(self,i,j):
		yend = i 
		xend = j
		res = []
		while yend < len(self.map):
			sum = 0
			xend = j
			while xend < len(self.map[0]):
				for k in range(i,yend+1):
					sum += self.map[k][xend]
				if sum == 10:
					res.append((yend,xend))
				elif sum >10:
					if yend != i and xend == j :
						 return res
					else:
						break
				xend += 1
			yend += 1

		return res

	def kjtes(self):
		time_start = time.time()
		time_end = time.time()

		while (time_end - time_start) <20:
			if not self.setps.is_empty():
				for t in range(int(self.setps.size() / 5 * 4)):
					self.backup()

			isfind = True
			while isfind:
				isfind = False
				i = 0
				while i < len(self.map):
					j = 0
					while j < len(self.map[0]):
						results = self.comput_ij(i,j)
						if len(results) >0 :
							for res in results:
								if(self.set_steps(i,j,res[0],res[1]) == True):
									isfind = True
									break
						j += 1
					i += 1
			
			new_map = np.array(self.map)
			cnt_array = np.where(new_map,0,1)
			score = np.sum(cnt_array)
			if score > self.score:
				print("------",score)
				self.score = score
				self.result = copy.deepcopy(self.current_result.items)

			time_end = time.time()
			if int(time_end- time_start ) % 10 == 0:
				print("耗时....",(time_end- time_start),self.score,self.max_size,self.min_size)
				print("------",self.score)

		# new_map = np.array(self.map)
		# cnt_array = np.where(new_map,0,1)
		# self.score = np.sum(cnt_array)
		# print(self.score)

	def auto_click(self):
		mouse = Controller()
		for res in self.result:
			si = res[0]
			sj = res[1]
			ei = res[2]
			ej = res[3]

			spos = self.game.get_Pix_Position(si,sj)
			epos = self.game.get_Pix_Position(ei,ej)
			spos = (spos[0] + self.x_start,spos[1] + self.y_start)
			epos = (epos[0] + self.x_start,epos[1] + self.y_start)

			mouse.position = (spos[0],spos[1])
			time.sleep(0.3)
			x, y = mouse.position
			mouse.press(Button.left)
			print(f"当前鼠标位置：({x}, {y})")
			
			mouse.position = (epos[0],epos[1])
			time.sleep(0.3)
			x, y = mouse.position
			print(f"当前鼠标位置：({x}, {y})")
			mouse.release(Button.left)

def main():
	result_level = 0
	labellists = []
	tes = Tes()
	tes.load_data("/Users/a58/Desktop/tes/img/4.jpg")
	
	tes.imagelists = np.array(tes.game.imagelists)
	tes.pre_img()

	min = backward.Minst()
	min.load_train_data()
	min.buildModel()
	min.train()
	min.evaluate()
	tes.map = min.predict(tes.imagelists).tolist()

	tes.ori_map = copy.deepcopy(tes.map) 

	tes.kjtes()
	tes.auto_click()

if __name__ == '__main__':
	main()