class Stack(object):
    # 初始化
    def __init__(self):
        self.items = []
    # 判断是否为空
    def is_empty(self):
        return self.items == []
    # 返回栈顶元素
    def peek(self):
        if self.is_empty():
            raise KeyError("stack is None")
        return self.items[len(self.items) - 1]
    #返回栈的大小
    def size(self):
        return len(self.items)
    # 入栈，把新的元素放进栈里
    def push(self, item):
        self.items.append(item)
    # 出栈，把栈顶元素丢出去
    def pop(self):
        if self.is_empty():
            raise KeyError("stack is None")
        return self.items.pop()