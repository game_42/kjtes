from tensorflow import keras
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Flatten,Conv2D,Dropout,Input,Dense,MaxPooling2D,BatchNormalization,MaxPool2D
from tensorflow.keras.optimizers import RMSprop
from sklearn import datasets
from sklearn.model_selection import train_test_split
import cv2
import numpy as np
import os
from tensorflow.keras.callbacks import *

earlystop_callback = EarlyStopping(
        monitor='val_precision', min_delta=0.008, mode='max',
        verbose=2, patience=1)

batch_size = 128
num_classes = 9
epochs = 100

class Minst:

    def __init__(self):
        self.model = Sequential()
        self.rootpath = "/Users/a58/Desktop/tes/"
        self.pre_datapath = "/Users/a58/Desktop/train/"
        self.file_path = "/Users/a58/Desktop/tes/model/"
        self.x_train = None
        self.y_train = None

    def read_img(self,img):
        image = cv2.imread(img)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = cv2.resize(image, (28, 28), interpolation=cv2.INTER_AREA)
        _, image = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY)

        return image

    def pre_pic(self,x_train,y_train):
        x_train = x_train.astype('float32')
        x_train /= 255.0

        y_train = keras.utils.to_categorical(y_train, num_classes)
        return x_train,y_train


    def load_train_data(self):
        x_train = []
        y_train = []
        train_path = os.path.join(self.rootpath,"train")
        train_path_file = os.path.join(train_path,"train.txt")
        with open(train_path_file , "r") as file:
            for line in file:
                words = line.split()
                img_path = os.path.join(train_path,words[0])
                x_train.append(self.read_img(img_path))
                y_train.append(int(words[1]) - 1)

        self.x_train = np.array(x_train)
        self.y_train = np.array(y_train)
        self.x_train,self.y_train = self.pre_pic(self.x_train,self.y_train)

        self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(self.x_train, self.y_train, test_size=0.25, random_state=1)
        print("dataset: train num:test num",len(self.x_train),len(self.x_test))

    def buildModel(self):
        self.model.add(Conv2D(input_shape=(28,28,1),filters=6, kernel_size=(5,5), strides=(1,1), padding='same', activation='relu'))
        self.model.add(MaxPool2D(pool_size=(2,2), strides=(2,2),padding='same'))
        self.model.add(Conv2D(filters=16, kernel_size=(5,5), strides=(1,1), padding='valid', activation='relu'))
        self.model.add(MaxPool2D(pool_size=(2,2), strides=(2,2),padding='same'))
        self.model.add(Conv2D(filters=120, kernel_size=(5,5), strides=(1,1), padding='valid', activation='relu'))
        self.model.add(MaxPool2D(pool_size=(2,2), strides=(2,2),padding='same'))
        self.model.add(Flatten())
        self.model.add(Dense(84, activation='relu'))
        self.model.add(Dense(9, activation='softmax'))
         # 形成网络（同时添加优化器）
        self.model.compile(loss='categorical_crossentropy',
                      optimizer=RMSprop(),
                      metrics=['accuracy'])

        self.load_model()
        # 查看网络结构，输出形状，参数个数
        print(self.model.summary())

    def load_model(self):
        self.model.load_weights(os.path.join(self.file_path + "adasd.h5"))

    def train(self):
        self.history = self.model.fit(self.x_train, self.y_train,
                            batch_size=batch_size,
                            epochs=epochs,
                            verbose=2,
                            validation_data=(self.x_test, self.y_test),
                            callbacks=keras.callbacks.EarlyStopping(monitor='loss',min_delta=0.002,patience=0,mode='auto',restore_best_weights=False))

        self.model.save_weights(os.path.join(self.file_path + "adasd.h5"))
        
    def evaluate(self):
        score = self.model.evaluate(self.x_test, self.y_test, verbose=0)
        
    def predict(self,imgs):
        predict = self.model.predict(imgs)
        index = np.argmax(predict,axis = 1)
        index=index.reshape(16,10)
        index += 1

        print(index)
        return index

def main():
    min = Minst()
    min.load_train_data()
    min.buildModel()
    min.train()
    min.evaluate()
    min.predict()

if __name__ == '__main__':
    main()